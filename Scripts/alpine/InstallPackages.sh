#!/bin/sh

apk add --quiet --no-cache  \
    build-base              \
    g++                     \
    make                    \
    automake                \
    wget                    \
    curl                    \
    tar                     \
    git                     \
    python-dev              \
    python3-dev             \
    cmake                   \
    ca-certificates         \
    musl-dev                \
    linux-headers           \
    ccache                  \
    libintl                 \
    icu

curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"

python get-pip.py
python3 get-pip.py

rm get-pip.py
