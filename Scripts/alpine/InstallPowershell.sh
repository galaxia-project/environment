#!/bin/sh

set -e

wget https://github.com/PowerShell/PowerShell/releases/download/v6.2.1/powershell-6.2.1-linux-alpine-x64.tar.gz
mkdir -p /opt/microsoft/powershell
tar -zxf powershell-6.2.1-linux-alpine-x64.tar.gz -C /opt/microsoft/powershell/
rm powershell-6.2.1-linux-alpine-x64.tar.gz
chmod +x /opt/microsoft/powershell/pwsh
ln -s /opt/microsoft/powershell/pwsh /bin/pwsh
pwsh --version

set +e

