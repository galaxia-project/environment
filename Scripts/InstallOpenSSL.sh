#!/bin/bash

set -e

git clone https://github.com/openssl/openssl.git
cd openssl

git checkout OpenSSL_1_1_1a
./config no-shared --prefix=/deps/openssl
make -j 4
make install

cd ..
rm -rf ./openssl

set +e
