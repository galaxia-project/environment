#!/bin/bash

set -e

wget https://github.com/Kitware/CMake/releases/download/v3.14.4/cmake-3.14.4-Linux-x86_64.sh -O ./cmake-install.sh
sh ./cmake-install.sh --prefix=/usr --skip-license
rm ./cmake-install.sh

set +e