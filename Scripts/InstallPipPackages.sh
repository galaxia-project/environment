#!/bin/bash

set -e

pip2 install requests gitpython
pip3 install requests gitpython

set +e
