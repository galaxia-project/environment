#!/bin/bash

set -e

git clone https://gitlab.com/galaxia-project/extern/google-breakpad.git
cd google-breakpad

git checkout xi-master
./configure
make -j 4
make install

cd ..
rm -rf ./google-breakpad

set +e