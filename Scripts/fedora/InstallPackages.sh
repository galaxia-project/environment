#!/bin/bash

set -e

dnf install -y                      \
    git                             \
    wget                            \
    curl                            \
    tar                             \
    make                            \
    automake                        \
    gcc                             \
    gcc-c++                         \
    libstdc++-static                \
    python-devel                    \
    python3                         \
    python2-pip                     \
    python3-pip

dnf clean all

set +e