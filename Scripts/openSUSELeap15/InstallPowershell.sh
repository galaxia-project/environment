#!/bin/bash

set -e

wget https://github.com/PowerShell/PowerShell/releases/download/v6.1.0/powershell-6.1.0-linux-x64.tar.gz -O powershell.tar.gz
mkdir -p /opt/microsoft/powershell
tar zxf powershell.tar.gz -C /opt/microsoft/powershell
rm powershell.tar.gz
chmod +x /opt/microsoft/powershell/pwsh
ln -s /opt/microsoft/powershell/pwsh /usr/bin/pwsh

set +e