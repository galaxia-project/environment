#!/bin/bash

set -e

zypper refresh
zypper --non-interactive install            \
    tar                                     \
    wget                                    \
    git                                     \
    python-devel                            \
    python-pip                              \
    python3                                 \
    python3-pip                             \
    libicu-devel                            \
    curl                                    \
    gzip

zypper --non-interactive install            \
    automake                                \
    make                                    \
    gcc8                                    \
    gcc8-c++
    
zypper clean -a

set +e