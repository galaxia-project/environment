#!/bin/bash

set -e

wget https://dl.bintray.com/boostorg/release/1.70.0/source/boost_1_70_0.tar.gz -O /boost.tar.gz
tar -xf boost.tar.gz
rm -f boost.tar.gz

git clone https://github.com/madler/zlib.git
cd zlib
git checkout v1.2.11
cd ..

cd boost_1_70_0

./bootstrap.sh --prefix=/deps/boost
./b2 --without-mpi link=static runtime-link=static -sZLIB_SOURCE=../zlib cxxstd=17 install -j 4 > /dev/null

cd ..

rm -rf boost_1_70_0
rm -rf zlib

set +e
