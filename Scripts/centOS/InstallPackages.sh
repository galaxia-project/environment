#!/bin/bash

set -e

yum -y update
yum install -y                      \
    wget                            \
    curl                            \
    tar                             \
    make                            \
    automake                        \
    python-devel                    \
    python-pip

yum -y install  https://centos7.iuscommunity.org/ius-release.rpm
yum -y install  git2u-all python36u python36u-libs python36u-devel python36u-pip
yum clean all

ln -s /usr/bin/python3.6 /usr/bin/python3

curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"

python get-pip.py
python3 get-pip.py

rm get-pip.py

set +e