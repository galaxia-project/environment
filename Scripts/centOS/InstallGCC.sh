#!/bin/bash

set -e

mkdir gcc
cd gcc

wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-8.1-1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-binutils-2.30-54.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-binutils-devel-2.30-54.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-build-8.1-1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-dwz-0.12-1.1.bs1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-dyninst-9.3.2-6.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-dyninst-devel-9.3.2-6.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-dyninst-doc-9.3.2-6.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-dyninst-static-9.3.2-6.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-dyninst-testsuite-9.3.2-6.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-elfutils-0.176-1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-elfutils-devel-0.176-1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-elfutils-libelf-0.176-1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-elfutils-libelf-devel-0.176-1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-elfutils-libs-0.176-1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-gcc-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-gcc-c++-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-gcc-gdb-plugin-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-gcc-gfortran-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-gcc-plugin-devel-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-gdb-8.2-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-gdb-gdbserver-8.2-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-libasan-devel-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-libatomic-devel-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-libgccjit-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-libgccjit-devel-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-libgccjit-docs-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-libitm-devel-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-liblsan-devel-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-libquadmath-devel-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-libstdc++-devel-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-libstdc++-docs-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-libtsan-devel-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-libubsan-devel-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-ltrace-0.7.91-1.bs1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-make-4.2.1-4.bs1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-memstomp-0.1.5-5.bs1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-oprofile-1.3.0-2.bs1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-oprofile-devel-1.3.0-2.bs1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-oprofile-jit-1.3.0-2.bs1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-perftools-8.1-1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-runtime-8.1-1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-strace-4.24-4.bs1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-systemtap-3.3-2.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-systemtap-client-3.3-2.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-systemtap-devel-3.3-2.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-systemtap-initscript-3.3-2.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-systemtap-runtime-3.3-2.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-systemtap-sdt-devel-3.3-2.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-systemtap-server-3.3-2.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-systemtap-testsuite-3.3-2.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-toolchain-8.1-1.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-valgrind-3.14.0-16.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/devtoolset-8-valgrind-devel-3.14.0-16.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/libasan5-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/liblsan-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/libtsan-8.3.1-3.el7.x86_64.rpm
wget --no-verbose https://cbs.centos.org/repos/sclo7-devtoolset-8-rh-candidate/x86_64/os/Packages/libubsan1-8.3.1-3.el7.x86_64.rpm

yum install -y *.rpm

cd ..
rm -rf gcc

set +e
