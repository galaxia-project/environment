#!/bin/bash

set -e

yum -y update
wget -q -O - https://packages.microsoft.com/config/rhel/7/prod.repo | tee /etc/yum.repos.d/microsoft.repo
yum install -y powershell
yum clean all

set +e