#!/bin/bash

set -e

apt update
apt install -y                      \
    software-properties-common

add-apt-repository -y ppa:ubuntu-toolchain-r/test
add-apt-repository -y ppa:git-core/ppa

set +e