#!/bin/bash

set -e

apt update
apt install -y                      \
    git                             \
    wget                            \
    tar                             \
    curl                            \
    make                            \
    automake                        \
    gcc-8                           \
    g++-8                           \
    python-dev                      \
    python-pip                      \
    python3-pip                     \
    apt-transport-https             \
    lsb-release
rm -rf /var/lib/apt/lists/*

set +e