FROM alpine:3.10

ADD ./Scripts /Scripts

RUN sh /Scripts/alpine/InstallPackages.sh               && \
    sh /Scripts/InstallPipPackages.sh                   && \
    sh /Scripts/InstallOpenSSL.sh                       && \
    sh /Scripts/InstallBoost.sh                         && \
    sh /Scripts/alpine/InstallPowershell.sh             && \
    rm -rf /Scripts
