FROM centos:7

ADD ./Scripts /Scripts

RUN sh /Scripts/centOS/InstallPackages.sh                      && \
    sh /Scripts/centOS/InstallGCC.sh                           && \
    sh /Scripts/InstallPipPackages.sh                          && \
    . /opt/rh/devtoolset-8/enable                              && \
    sh /Scripts/InstallCMake.sh                                && \
    sh /Scripts/InstallBreakpad.sh                             && \
    sh /Scripts/InstallOpenSSL.sh                              && \
    sh /Scripts/InstallBoost.sh                                && \
    sh /Scripts/centOS/InstallPowershell.sh                    && \
    cp /Scripts/centOS/Entrypoint.sh /usr/bin/entrypoint.sh    && \
    chmod +x /usr/bin/entrypoint.sh                            && \
    rm -rf /Scripts

ENTRYPOINT [ "/usr/bin/entrypoint.sh" ]
