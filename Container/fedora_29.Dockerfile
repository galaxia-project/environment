FROM fedora:29

ADD ./Scripts /Scripts

RUN sh /Scripts/fedora/InstallPackages.sh               && \
    sh /Scripts/InstallPipPackages.sh                   && \
    sh /Scripts/InstallCMake.sh                         && \
    sh /Scripts/InstallBreakpad.sh                      && \
    sh /Scripts/InstallOpenSSL.sh                       && \
    sh /Scripts/InstallBoost.sh                         && \
    sh /Scripts/fedora/InstallPowershell.sh             && \
    rm -rf /Scripts
