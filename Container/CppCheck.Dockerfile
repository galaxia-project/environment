FROM registry.gitlab.com/galaxia-project/environment/ubuntu:bionic

RUN apt update                     && \
  apt install -y                    \
  cppcheck                        \
  python                          \
  python-pip                   && \
  rm -rf /var/lib/apt/lists/*    && \
  pip install pygments
