FROM ubuntu:bionic

ADD ./Scripts /Scripts

RUN sh /Scripts/Ubuntu/InstallPackages.sh               && \
    sh /Scripts/InstallPipPackages.sh                   && \
    sh /Scripts/UpdateAlternatives.sh                   && \
    sh /Scripts/InstallCMake.sh                         && \
    sh /Scripts/InstallBreakpad.sh                      && \
    sh /Scripts/InstallOpenSSL.sh                       && \
    sh /Scripts/InstallBoost.sh                         && \
    sh /Scripts/Ubuntu/InstallPowershell.sh             && \
    rm -rf /Scripts
